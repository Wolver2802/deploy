import React from 'react';
import './Loader.css';

/**
 * @author
 * @function Loader
 **/

export const Loader = (props) => {
  return (
    <div className='sceleton-wrapper'>
      <div className='sceleton'></div>
      <div className='sceleton'></div>
      <div className='sceleton'></div>
      <div className='sceleton'></div>
      <div className='sceleton'></div>
      <div className='sceleton'></div>
    </div>
  );
};
