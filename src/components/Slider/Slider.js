import React from 'react';
import './Slider.css';
import small_plant from '../../image/plantSmall.png';
import big_plant from '../../image/plantBig.png';
import { Button } from '../../utils/Button/Button';
import { Link } from 'react-router-dom';

/**
 * @author
 * @function Slider
 **/

export const Slider = (props) => {
  return (
    <div className='slider-wrapper'>
      <div className='labels'>
        <h2 className='labels__small'>Welcome to GreenShop</h2>
        <h1 className='labels__big'>
          Let’s Make a Better <span className='labels__big_green'>Planet</span>
        </h1>
        <p className='labels__content'>
          We are an online products shop offering a wide range of cheap and
          trendy products. Use our products to create an unique Urban Jungle.
          Order your favorite products!
        </p>
        <Button className='button_green'>
          <Link className='labels__link' to={'/'}>
            SHOP NOW
          </Link>
        </Button>
      </div>
      <div className='slider-wrapper__img'>
        <img src={small_plant} alt='small_plant' className='small_plant' />
        <img src={big_plant} alt='big_plant' className='big_plant' />
      </div>
    </div>
  );
};
