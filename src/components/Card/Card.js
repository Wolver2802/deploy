import './Card.css';

export const Card = ({ title, price, image }) => {
  return (
    <div className={'card'}>
      <div className='card__image'>
        <img src={image} alt='avatar' />
      </div>
      <h2 className={'card__title'}>{title}</h2>
      <span className={'card__price'}>$ {price}</span>
    </div>
  );
};
