import React, { useEffect, useState } from 'react';
import { CardList } from '../CardList/CardList';
import { Loader } from '../Loader/Loader';
import './Main.css';

/**
 * @author
 * @function Main
 **/

export const Main = (props) => {
  const [state, setState] = useState({});
  useEffect(() => {
    fetch('https://fakestoreapi.com/products')
      .then((res) => {
        if (!res.ok) {
          throw new Error(res.statusText);
        }
        return res.json();
      })
      .catch((err) => {
        console.log(err);
      })
      .then((res) => {
        setState({ products: res });
      });
  }, []);

  const renderUsers = () => {
    if (state.products?.length) {
      return <CardList productList={state.products} />;
    }

    return <Loader />;
  };

  return (
    <main className='main'>
      <h1 className='main__header'>Главная страница</h1>
      {renderUsers()}
    </main>
  );
};
