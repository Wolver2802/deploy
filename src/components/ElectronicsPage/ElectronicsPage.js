import React, { useEffect } from 'react';
import { useState } from 'react/cjs/react.development';
import { CardList } from '../CardList/CardList';
import { Loader } from '../Loader/Loader';
import './ElectronicsPage.css';

/**
 * @author
 * @function ElectronicsPage
 **/

export const ElectronicsPage = (props) => {
  const [state, setState] = useState({});
  useEffect(() => {
    fetch('https://fakestoreapi.com/products/category/electronics')
      .then((res) => {
        if (!res.ok) {
          throw new Error(res.statusText);
        }
        return res.json();
      })
      .catch((err) => {
        console.log(err);
      })
      .then((res) => {
        setState({ products: res });
      });
  }, []);

  const renderUsers = () => {
    if (state.products) {
      return <CardList productList={state.products} />;
    }

    return <Loader />;
  };

  return (
    <div className='main'>
      <h2 className='main__header'>Electronics Page</h2>
      {renderUsers()}
    </div>
  );
};
