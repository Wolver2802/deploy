import { Card } from '../Card/Card';
import { Link } from 'react-router-dom';
import './CardList.css';

export const CardList = (props) => {
  return (
    <div className={'card-list'}>
      {props.productList.map((product) => {
        return (
          <div className={'card-list__item'} key={product.id}>
            <Link to={`${product.id}`}>
              <Card
                id={product.id}
                title={product.title}
                price={product.price}
                image={product.image}
              />
            </Link>
          </div>
        );
      })}
    </div>
  );
};
