import React from 'react';
import './Button.css';

/**
 * @author
 * @function Button
 **/

export const Button = ({ children, ...props }) => {
  return (
    <button {...props} className='button_green'>
      {children}
    </button>
  );
};
